#include "pwm_motor_control.h"

static uint16_t internal_period;
static uint8_t internal_prescaler;

TIM_OC_InitTypeDef sPWMConfig;

/* TIM handle declaration */
TIM_HandleTypeDef    TimHandle3;

void Init_PWM_TIM3_CH1(uint16_t initial_period, uint8_t prescaler_value){

  internal_period = initial_period;
  internal_prescaler = prescaler_value;

  // D3 is on Port B - Bit 4 
  GPIO_InitTypeDef  gpio_init_structure;

  /* Enable the GPIO_B clock */
    __HAL_RCC_GPIOB_CLK_ENABLE();

  /* Configure the GPIOG_6 pin */
    gpio_init_structure.Pin = GPIO_PIN_4;
    gpio_init_structure.Mode = GPIO_MODE_AF_PP;
    gpio_init_structure.Pull = GPIO_PULLDOWN;
    gpio_init_structure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	  gpio_init_structure.Alternate = GPIO_AF2_TIM3;

   HAL_GPIO_Init(GPIOB, &gpio_init_structure);
  
    /* Set TIMx instance */
    TimHandle3.Instance = TIM3;

    /* Initialize TIMx peripheral as follows:
        + Period = 10000 - 1
        + Prescaler = ((SystemCoreClock / 2)/10000) - 1
        + ClockDivision = 0
        + Counter direction = Up
    */
    TimHandle3.Init.Prescaler         = prescaler_value;
    TimHandle3.Init.Period            = initial_period;
    TimHandle3.Init.ClockDivision     = 0;
    TimHandle3.Init.CounterMode       = TIM_COUNTERMODE_UP;
    TimHandle3.Init.RepetitionCounter = 0;
    TimHandle3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_PWM_Init(&TimHandle3) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }


  /*##-2- Configure the PWM channels #########################################*/ 
  /* Common configuration for all channels */
  sPWMConfig.OCMode      = TIM_OCMODE_PWM2;
  sPWMConfig.OCFastMode  = TIM_OCFAST_DISABLE;
  sPWMConfig.OCPolarity  = TIM_OCPOLARITY_LOW;
  sPWMConfig.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sPWMConfig.OCIdleState = TIM_OCIDLESTATE_SET;
  sPWMConfig.OCNIdleState= TIM_OCNIDLESTATE_RESET;
}

void Set_PWM_value(uint16_t period_pwm, uint16_t duty_cycle_pulse){

  if(period_pwm != internal_period){
    internal_period = period_pwm;
    TimHandle3.Init.Prescaler         = internal_prescaler;
    TimHandle3.Init.Period            = period_pwm;
    TimHandle3.Init.ClockDivision     = 0;
    TimHandle3.Init.CounterMode       = TIM_COUNTERMODE_UP;
    TimHandle3.Init.RepetitionCounter = 0;
    TimHandle3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_PWM_Init(&TimHandle3) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }
  }
  sPWMConfig.Pulse = duty_cycle_pulse;
  if(HAL_TIM_PWM_ConfigChannel(&TimHandle3, &sPWMConfig, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Configuration Error */
    Error_Handler();
  }
  
  if(HAL_TIM_PWM_Start(&TimHandle3, TIM_CHANNEL_1) != HAL_OK)
  {
    /* Starting Error */
    Error_Handler();
  }
}

static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
  }
}
