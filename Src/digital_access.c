#include "digital_access.h"

void Init_IO_Pin_D5(void){

  GPIO_InitTypeDef  gpio_init_structure;
	
	
	/* Enable the GPIO clock */
	__HAL_RCC_GPIOI_CLK_ENABLE();

	/* Configure the GPIO pin D5 */
	gpio_init_structure.Pin = GPIO_PIN_0;
	gpio_init_structure.Mode = GPIO_MODE_INPUT;
	gpio_init_structure.Pull = GPIO_PULLDOWN;
	gpio_init_structure.Speed = GPIO_SPEED_HIGH;

	HAL_GPIO_Init(GPIOI, &gpio_init_structure);

}


void Init_IO_Pin_D7(void){

	GPIO_InitTypeDef  gpio_init_structure;
	
	
	/* Enable the GPIO clock */
	__HAL_RCC_GPIOI_CLK_ENABLE();

	/* Configure the GPIO pin D7 */
	gpio_init_structure.Pin = GPIO_PIN_3;
	gpio_init_structure.Mode = GPIO_MODE_INPUT;
	gpio_init_structure.Pull = GPIO_PULLDOWN;
	gpio_init_structure.Speed = GPIO_SPEED_HIGH;

	HAL_GPIO_Init(GPIOI, &gpio_init_structure);

}

uint8_t ReadDigitalPin_D5(void){
  return (uint8_t)(HAL_GPIO_ReadPin(GPIOI, GPIO_PIN_0));
}

 uint8_t ReadDigitalPin_D7(void){
   return (uint8_t)(HAL_GPIO_ReadPin(GPIOI, GPIO_PIN_3));
 }

