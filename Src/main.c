/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "ethernetif.h"
#include "lwip/netif.h"
#include "lwip/tcpip.h"
#include "app_ethernet.h"
#include "httpserver-netconn.h"
#include "lcd_log.h"
#include "pwm_motor_control.h"
#include "image.h"
#include "type_define.h"
#include "digital_access.h"
#include "qr_encode.h"
#include <semphr.h>

extern SemaphoreHandle_t xMutexQrCode;

/* Private define*/
#define USE_ETHERNET

/* Private variables ---------------------------------------------------------*/
static TS_StateTypeDef  TS_State;
struct netif gnetif; /* network interface structure */
osThreadId pwm_motor_control_thread_Id;
osThreadId touchscreen_thread_Id;
osThreadId state_thread_Id;

TStateBox2 StateBox;
TStateBox2 StateBox_old;

#ifndef USE_FULL_ASSERT
uint32_t    ErrorCounter = 0;
#endif

/* OS message declaration*/

osMessageQDef(rest_api_message, 1, uint8_t); 
osMessageQId (rest_api_message);   

char uuid[37];
char copyIsFinish = 0;

   const uint32_t pwm_period = 21700; //1000Hz
   const uint32_t pwm_pulse_forward_50 = (737 * (21700 - 1)) / 10000; //; Forward
   const uint32_t pwm_pulse_backward_50 = (645 * (21700 - 1)) / 10000; //; Backward
   const uint32_t pwm_pulse_forward_100 = (783 * (21700 - 1)) / 10000; //; Forward
   const uint32_t pwm_pulse_backward_100 = (599 * (21700 - 1)) / 10000; //; Backward
   const uint8_t prescaler = 100; //; Forward
 

/* Private function prototypes -----------------------------------------------*/
static void SystemClock_Config(void);
static void StartThread(void const * argument);
static void touchscreen_demo(void const *argument);
static void Netif_Config(void);
static void MPU_Config(void);
static void Error_Handler(void);
static void CPU_CACHE_Enable(void);
static void statechart_thread(void const *argument);
void QRGenerator(char *input,uint8_t scale_qr, uint8_t version);
void displayQRCode(int side, uint8_t *bitdata,uint8_t scale);

void displayQRCode(int side, uint8_t *bitdata, uint8_t scale)
{
	BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
	BSP_LCD_FillRect(0,0,BSP_LCD_GetXSize(),BSP_LCD_GetYSize());
	int i=0;
	int j=0;
	int a=0;
	int l=0;
	int n=0;
	int w,x;
	int OUT_FILE_PIXEL_PRESCALER=1;
	if (side==21)
	{
		OUT_FILE_PIXEL_PRESCALER=2;
	}
		for (i = 0; i < side; i++) {

			for (j = 0; j < side; j++) {
				a = j * side + i;

				if ((bitdata[a / 8] & (1 << (7 - a % 8))))
				{
					for (l = 0; l < OUT_FILE_PIXEL_PRESCALER; l++)
					{
						for (n = 0; n < OUT_FILE_PIXEL_PRESCALER; n++)
						{
							for(w=0;w<scale;w++){
								for(x=0;x<scale;x++){
									BSP_LCD_DrawPixel(BSP_LCD_GetXSize()/2-side/2*scale+OUT_FILE_PIXEL_PRESCALER*i*scale+l*scale+w, BSP_LCD_GetYSize()/2-side/2*scale+OUT_FILE_PIXEL_PRESCALER*(j*scale)+n*scale+x, 0xFF0000);
								}
							}
						}
					}
				}
			}
		}
}

void QRGenerator(char *input,uint8_t scale_qr, uint8_t version)
{
	int side;
	uint8_t bitdata[QR_MAX_BITDATA];

	// remove newline
	if (input[strlen(input) - 1] == '\n')
	{
		input[strlen(input) - 1] = 0;
	}

	side = qr_encode(QR_LEVEL_L, version, input, 0, bitdata);
	displayQRCode(side, bitdata, scale_qr);
}

static void touchscreen_demo(void const *argument)
{
 uint8_t  status = 0;
  uint16_t x, y;
  uint8_t  text[30];

  status = BSP_TS_Init(BSP_LCD_GetXSize(), BSP_LCD_GetYSize());
  while (1)
  {
    if (status == TS_OK)
    {
      /* Check in polling mode in touch   screen the touch status and coordinates */
      /* if touch occurred                                                      */
      BSP_TS_GetState(&TS_State);
      if(TS_State.touchDetected)
      {
        /* Get X and Y position of the touch post calibrated */
        x = TS_State.touchX[0];
        y = TS_State.touchY[0];
        /* Display 1st touch detected coordinates */
        sprintf((char*)text, "TS detected : 1[%d,%d] \n", x, y);
        LCD_UsrLog ((char *)&text);
        if(x<BSP_LCD_GetXSize()/2){
          if(y<BSP_LCD_GetYSize()/2){
            LCD_UsrLog ((char *)" TS  : API Rest - OPEN Box Order \n");
            osMessagePut(rest_api_message, OPEN_BOX, 0);
          }
          else{
            LCD_UsrLog ((char *)" TS  : API Rest - CLOSE Box Order \n");
            osMessagePut(rest_api_message, CLOSE_BOX, 0);
          }
        }else{
					//LCD_UsrLog ((char *)" QR  : Encode - Start... \n");
					//QRGenerator("012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789",6,3);
					//LCD_UsrLog ((char *)" QR  : Encode - Finish !\n");
				}
      } 
    }
    osDelay(25);
  }
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if(htim->Instance==TIM6){
			HAL_IncTick();
	}
	}

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */

int main(void)
{
  /* Configure the MPU attributes as Device memory for ETH DMA descriptors */
  MPU_Config();

  /* Enable the CPU Cache */
   CPU_CACHE_Enable();

  /* STM32F7xx HAL library initialization:
       - Configure the Flash ART accelerator on ITCM interface
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Global MSP (MCU Support Package) initialization
     */
  HAL_Init();  
  
  /* Configure the system clock to 200 MHz */
  SystemClock_Config(); 

  /* Init thread */
#if defined(__GNUC__)
  osThreadDef(Start, StartThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 10);
#else
  osThreadDef(Start, StartThread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 5);
#endif
  osThreadCreate (osThread(Start), NULL);

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */
  for( ;; );
}


/**
  * @brief  Initializes the STM327546G-Discovery's LCD  resources.
  * @param  None
  * @retval None
  */
void BSP_Config(void)
{
  /* Initialize the LCD */
  BSP_LCD_Init();

  /* Initialize the LCD Layers */
  BSP_LCD_LayerDefaultInit(1, LCD_FB_START_ADDRESS);

  /* Set LCD Foreground Layer  */
  BSP_LCD_SelectLayer(1);

  BSP_LCD_SetFont(&LCD_DEFAULT_FONT);

  /* Initialize LCD Log module */
  LCD_LOG_Init();

  /* Show Header and Footer texts */
  LCD_LOG_SetHeader((uint8_t *)"Drooniveroo_camera");
  LCD_LOG_SetFooter((uint8_t *)"CPE Engineering School");
}

void BSP_LogShow(void){
	BSP_LCD_SelectLayer(1);
  BSP_LCD_SetFont(&LCD_DEFAULT_FONT);
  /* Initialize LCD Log module */
  LCD_LOG_Init();
  /* Show Header and Footer texts */
  LCD_LOG_SetHeader((uint8_t *)"Drooniveroo_camera");
  LCD_LOG_SetFooter((uint8_t *)"CPE Engineering School");
}

/**
  * @brief  Start Thread 
  * @param  argument not used
  * @retval None
  */
static void StartThread(void const * argument)
{ 
  /* Initialize LCD */
  BSP_Config();
  
  #ifdef USE_ETHERNET
	
	LCD_UsrLog ((char *)"Initialisation d'internet...\r\n");
  // Create tcp_ip stack thread 
  tcpip_init(NULL, NULL);
  
  // Initialize the LwIP stack 
  Netif_Config();
  
  // Initialize webserver demo 
  http_server_netconn_init();
  #endif
  
  // Notify user about the network interface config 
  User_notification(&gnetif);

  //Init IO access
  Init_IO_Pin_D5(); // Front BOX pressure sensor
  Init_IO_Pin_D7(); // Rear BOX pressure sensor
 
#ifdef USE_DHCP
  // Start DHCPClient
  osThreadDef(DHCP, DHCP_thread, osPriorityNormal, 0, configMINIMAL_STACK_SIZE * 4);
  osThreadCreate (osThread(DHCP), &gnetif);
#endif

  osThreadDef(state_chart, statechart_thread,osPriorityNormal, 0, configMINIMAL_STACK_SIZE*10);
  osThreadDef(touchscreen,touchscreen_demo,osPriorityBelowNormal,0, configMINIMAL_STACK_SIZE*20);

  state_thread_Id = osThreadCreate(osThread(state_chart),NULL);
  touchscreen_thread_Id = osThreadCreate(osThread(touchscreen),NULL);

  // Create messages queu
   rest_api_message = osMessageCreate(osMessageQ(rest_api_message), NULL);

  for( ;; )
  {
    /* Delete the Init Thread */ 
    osThreadTerminate(NULL);
  }
}

/**
  * @brief  Initializes the lwIP stack
  * @param  None
  * @retval None
  */
static void Netif_Config(void)
{ 
  ip_addr_t ipaddr;
  ip_addr_t netmask;
  ip_addr_t gw;
 
#ifdef USE_DHCP
  ip_addr_set_zero_ip4(&ipaddr);
  ip_addr_set_zero_ip4(&netmask);
  ip_addr_set_zero_ip4(&gw);
#else
  IP_ADDR4(&ipaddr,IP_ADDR0,IP_ADDR1,IP_ADDR2,IP_ADDR3);
  IP_ADDR4(&netmask,NETMASK_ADDR0,NETMASK_ADDR1,NETMASK_ADDR2,NETMASK_ADDR3);
  IP_ADDR4(&gw,GW_ADDR0,GW_ADDR1,GW_ADDR2,GW_ADDR3);
#endif /* USE_DHCP */
  
  netif_add(&gnetif, &ipaddr, &netmask, &gw, NULL, &ethernetif_init, &tcpip_input);
  
  /*  Registers the default network interface. */
  netif_set_default(&gnetif);
  
  if (netif_is_link_up(&gnetif))
  {
    /* When the netif is fully configured this function must be called.*/
    netif_set_up(&gnetif);
  }
  else
  {
    /* When the netif link is down this function must be called */
    netif_set_down(&gnetif);
  }
}

static void statechart_thread(void const *argument)
{
  osEvent  osEvent_APIRest;
	uint8_t api_rest_value;
	
  Init_PWM_TIM3_CH1(pwm_period,prescaler);
  StateBox = CLOSED_BOX_WAIT_REST_ID;
	StateBox_old = OPENING_BOX_PWM_BACKWARD;
  while(1){
    // Testing input
    switch(StateBox){
      case CLOSED_BOX_WAIT_REST_ID:
				//Si capteur avant enfonc�
				osEvent_APIRest = osMessageGet(rest_api_message,20);
						if (osEvent_APIRest.status == osEventMessage){
							api_rest_value = osEvent_APIRest.value.v;
							if (api_rest_value == OPEN_BOX) {
								StateBox = OPENING_BOX_PWM_BACKWARD;
							}else if( api_rest_value == CLOSE_BOX){
								StateBox = CLOSING_BOX_PWM_FORWARD;
							}
						}
				if (ReadDigitalPin_D5() == 0) {
					// Wait API REST Q		
					//xSemaphoreTake(xMutexQrCode, portMAX_DELAY);
          if (copyIsFinish != 0) {
            StateBox = CLOSED_BOX_WAIT_QR_CONFIRMATION;
            copyIsFinish = 0;
          }
					//xSemaphoreGive(xMutexQrCode);
				}
      break;
      case CLOSED_BOX_WAIT_QR_CONFIRMATION:
				osEvent_APIRest = osMessageGet(rest_api_message,20);
        if (osEvent_APIRest.status == osEventMessage){
          api_rest_value = osEvent_APIRest.value.v;
          if (api_rest_value == OPEN_BOX) {
            StateBox = OPENING_BOX_PWM_BACKWARD;
          }
        }
      break;
      case OPENING_BOX_PWM_BACKWARD:
        if(ReadDigitalPin_D5() == 1){
          StateBox = OPENED_BOX_WAIT_ORDER_CLOSED;
        }
      break;
      case OPENED_BOX_WAIT_ORDER_CLOSED:
      // Wait for REST API to get event to close the box
      osEvent_APIRest = osMessageGet(rest_api_message,20);
        if (osEvent_APIRest.status == osEventMessage){
          api_rest_value = osEvent_APIRest.value.v;
					if (api_rest_value == CLOSE_BOX) {
						StateBox = CLOSING_BOX_PWM_FORWARD;
					}
        }
      break;
      case CLOSING_BOX_PWM_FORWARD:
        if(ReadDigitalPin_D7()==0){
          StateBox = CLOSED_BOX_WAIT_REST_ID;
        }
      break;
    }

    // Performing action 
   if(StateBox!=StateBox_old){
      StateBox_old = StateBox;
      switch(StateBox){
        case CLOSED_BOX_WAIT_REST_ID:
          // Stop PWM
          LCD_UsrLog ((char *)" Statechart : Wait for QR code REST API : /qrcode/{id} \n");
          Set_PWM_value(pwm_period,0);
        break;
        case CLOSED_BOX_WAIT_QR_CONFIRMATION:
          // Show QR Code
          LCD_UsrLog ((char *)" Statechart : Show QR Code - Wait for confirmation \n");
          QRGenerator(uuid, 9, 3);
        break;
        case OPENING_BOX_PWM_BACKWARD:
					BSP_LogShow();
          LCD_UsrLog ((char *)" Statechart : Opening box - PWM Forward \n");
          // Opening BOX - Continous PWM FORWARD
          Set_PWM_value(pwm_period,pwm_pulse_forward_100);
        break;
        case OPENED_BOX_WAIT_ORDER_CLOSED:
          LCD_UsrLog ((char *)" Statechart : Wait for order CLOSE from API REST\n");
          Set_PWM_value(pwm_period,0);
        break;
        case CLOSING_BOX_PWM_FORWARD:
          LCD_UsrLog ((char *)" Statechart : Closing box - PWM Backward \n");
          // Closing BOX - Continous PWM FORWARD
          Set_PWM_value(pwm_period,pwm_pulse_backward_100);
        break;
        default:
        break;
        }
    }
    osDelay(10);
  }
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 200000000
  *            HCLK(Hz)                       = 200000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 25000000
  *            PLL_M                          = 25
  *            PLL_N                          = 432
  *            PLL_P                          = 2
  *            PLL_Q                          = 9
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 7
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_OFF;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 400;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /* activate the OverDrive */
  if(HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;  
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* User may add here some code to deal with this error */
  while(1)
  {
  }
}

/**
  * @brief  Configure the MPU attributes as Device for  Ethernet Descriptors in the SRAM1.
  * @note   The Base Address is 0x20010000 since this memory interface is the AXI.
  *         The Configured Region Size is 256B (size of Rx and Tx ETH descriptors) 
  *       
  * @param  None
  * @retval None
  */
static void MPU_Config(void)
{
  MPU_Region_InitTypeDef MPU_InitStruct;
  
  /* Disable the MPU */
  HAL_MPU_Disable();
  
  /* Configure the MPU attributes as Device for Ethernet Descriptors in the SRAM */
  MPU_InitStruct.Enable = MPU_REGION_ENABLE;
  MPU_InitStruct.BaseAddress = 0x20010000;
  MPU_InitStruct.Size = MPU_REGION_SIZE_256B;
  MPU_InitStruct.AccessPermission = MPU_REGION_FULL_ACCESS;
  MPU_InitStruct.IsBufferable = MPU_ACCESS_BUFFERABLE;
  MPU_InitStruct.IsCacheable = MPU_ACCESS_NOT_CACHEABLE;
  MPU_InitStruct.IsShareable = MPU_ACCESS_SHAREABLE;
  MPU_InitStruct.Number = MPU_REGION_NUMBER0;
  MPU_InitStruct.TypeExtField = MPU_TEX_LEVEL0;
  MPU_InitStruct.SubRegionDisable = 0x00;
  MPU_InitStruct.DisableExec = MPU_INSTRUCTION_ACCESS_ENABLE;

  HAL_MPU_ConfigRegion(&MPU_InitStruct);

  /* Enable the MPU */
  HAL_MPU_Enable(MPU_PRIVILEGED_DEFAULT);
}

/**
  * @brief  CPU L1-Cache enable.
  * @param  None
  * @retval None
  */
static void CPU_CACHE_Enable(void)
{
  /* Enable I-Cache */
  SCB_EnableICache();

  /* Enable D-Cache */
  SCB_EnableDCache();
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
