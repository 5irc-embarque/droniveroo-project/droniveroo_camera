#ifndef __digital_access
#define __digital_access

#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "cmsis_os.h"

void Init_IO_Pin_D5(void);
void Init_IO_Pin_D4(void);
void Init_IO_Pin_D7(void);
void Init_IO_Pin_D8(void);
void Init_IO_Pin_D13(void);
void Init_IO_Pin_D15(void);
uint8_t ReadDigitalPin_D1(void);
uint8_t ReadDigitalPin_D4(void);
uint8_t ReadDigitalPin_D7(void);
uint8_t ReadDigitalPin_D8(void);
uint8_t ReadDigitalPin_D5(void);

#endif
