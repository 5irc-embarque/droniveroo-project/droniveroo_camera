#ifndef __PWM_motor_control
#define __PWM_motor_control

#include "stm32f7xx_hal.h"
#include "stm32746g_discovery.h"
#include "cmsis_os.h"

void Set_PWM_value(uint16_t period_pwm, uint16_t alpha_pulse);
void Init_PWM_TIM3_CH1(uint16_t initial_period, uint8_t prescaler_value);
//void Full_PWM_Test(void);

static void Error_Handler(void);


#endif
