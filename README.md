
# Dronivero_camera
**Repos qui contient le code source du projet utilisé pour la carte de découverte STM32**  
Le projet est à placer dans :

C:\STM32Cube_FW_F7_V1.8.0\Projects\STM32746G-Discovery\Applications\LwIP\

## Cablage des des entrées/sorties de la carte discovery STM32F-Discovery
Les GPIO de la carte de découverte utilisés sont (Broche carte découverte -> Port STM32 -> Fonction ) : 
- Arduino D3/PWM -> PB4 -> Commande PWM servomoteur Parallax 
- Arduino D5 -> PI0 -> Capteur de fin de course avant 
- Arduino D7 -> PI3 -> Capteur de fin de course arrière

## Specifitées du projet
- IDE : Keil µVision 5.24.2.0
- Utilisation OS : FreeRtOS et CMSIS
- Utilisation des librairies de la carte de découverte : camera, écran, tactile...
- Fonctionnement selon machine d'état => voir thread `statechart_thread()`

## Architecture du projet

```
/Src => Contient les fichiers source C des librairies utilisées / implémentées
/Inc => Contient les fichiers header des librairies utilisées / implémentées
/MDK-ARM => Répertoirement de lancement du projet keil (Project)
/zbar => Librairie ZBar (non-utilisée/dépréciée) au finale
```

## Librairies / import utilisés
- camera.c => caméra carte de découverte
- Serveur Web/DHCP http_server-netcon.c => Thread de gestion du serveur HTTP et du routage
- qr_encode.c => Librarie d'encodage du QR Code
- pwm_motor-control => Librairie de gestion 
## RtOS - Thread
- StartThread => Thread de démarrage, lance les threads de config et les autres threads
- statechart_thread => Thread de la machine d'état
- Netif_Config => Initialisation de la stack lwIP stack
- touchscreen_demo => Thread de gestion de l'écran tactle

## RtOs - Messages
- rest_api_message => Communication entre le thread de la machine d'état et celui du serveur Web. Permet l'envoie d'ordres à la machine d'état

## Route du serveur Web
- /qrcode/{uuid} => Réception de la valeur du QR Code
- /openBox => Ordre d'ouverture de la boîte
- /closeBox => Ordre de fermeture de la boîte
- /isOpen => Permet de savoir si la boîte est ouverte (1:Open)
- /isClose => Permet de savoir si la boîte est fermée (1:Close)

Voir fichier `httpserver-netconn.c` pour les détails



